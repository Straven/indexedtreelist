package main.java.com.straven.datastructures.collections;

import java.util.*;

public class IndexedTreeListSet<E> extends AbstractIndexedTreeList<E> implements Set<E> {

    protected final Map<E, AVLNode> nodeMap;

    public IndexedTreeListSet() {
        this(new HashMap<>());
    }

    public IndexedTreeListSet(final Map map) {
        this.nodeMap = map;
    }

    public IndexedTreeListSet(final Collection<? extends E> coll) {
        this(coll, new HashMap<>());
    }

    public IndexedTreeListSet(final Collection<? extends E> coll, final Map map) {
        this.nodeMap = map;
        for (E e : coll) {
            add(e);
        }
    }

    @Override
    public int indexOf(final Object object) {
        AVLNode node = nodeMap.get(object);
        if (node == null) {
            return -1;
        }
        return node.getPosition();
    }

    @Override
    public int lastIndexOf(final Object object) {
        return indexOf(object);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return Collections.unmodifiableList(super.subList(fromIndex, toIndex));
    }

    @Override
    public boolean contains(final Object object) {
        return nodeMap.containsKey(object);
    }

    @Override
    public E set(int index, final E obj) {
        final int pos = indexOf(obj);
        if (pos >= 0 && pos != index) {
            remove(pos);
            if (pos < index) {
                index--;
            }
        }
        return super.set(index, obj);
    }

    public void clear() {
        super.clear();
        nodeMap.clear();
    }

    @Override
    protected boolean canAdd(E e) {
        if (e == null) {
            throw new NullPointerException("Null elements are not allowed");
        }
        return !nodeMap.containsKey(e);
    }

    @Override
    protected void addNode(AVLNode node) {
        nodeMap.put(node.getValue(), node);
    }

    @Override
    protected void removeNode(AVLNode node) {
        nodeMap.remove(node.getValue());
    }
}
