package main.java.com.straven.datastructures.collections;

import java.util.*;

abstract class AbstractIndexedTreeList<E> extends AbstractList<E> {
    protected AVLNode root;

    protected int size =0;

    boolean supportSetInIterator = true;

    @Override
    public E get(final int index) {
        return getNode(index).getValue();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<E> listIterator(final int fromIndex) {
        checkInterval(fromIndex, 0, size());
        return new TreeListIterator(this, fromIndex);
    }

    @Override
    public Object[] toArray() {
        final Object[] array = new Object[size()];
        if (root != null) {
            root.toArray(array, root.relativePosition);
        }
        return array;
    }

    @Override
    public boolean add(E e) {
        if (!canAdd(e)) {
            return false;
        }
        return super.add(e);
    }

    @Override
    public void add(final int index, final E obj) {
        if (!canAdd(obj)) {
            return;
        }
        modCount++;
        checkInterval(index, 0, size());
        if (root == null) {
            setRoot(new AVLNode(index, obj, null, null, null));
        } else {
            setRoot(root.insert(index, obj));
        }
        size++;
    }

    @Override
    public boolean addAll(final int index, final Collection<? extends E> collection) {
        modCount++;
        checkInterval(index, 0, size());

        int currentIndex = index;

        for (E obj : collection) {
            if (canAdd(obj)) {
                if (root == null) {
                    setRoot(new AVLNode(currentIndex, obj, null, null, null));
                } else {
                    setRoot(root.insert(currentIndex, obj));
                }
                size++;
                currentIndex++;
            }
        }

        return currentIndex > index;
    }

    @Override
    public E set(final int index, final E obj) {
        final AVLNode node = getNode(index);
        final E result = node.value;
        node.setValue(obj);
        return result;
    }

    @Override
    public E remove(final int index) {
        modCount++;
        checkInterval(index, 0, size() -1);
        final E result = get(index);
        setRoot(root.remove(index));
        size--;
        return result;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index < 0 ) {
            return false;
        }
        remove(index);
        return true;
    }

    @Override
    public void clear() {
        modCount++;
        root = null;
        size = 0;
    }

    @Override
    public Spliterator<E> spliterator() {
        return Spliterators.spliterator(this, Spliterator.ORDERED);
    }

    private AVLNode getNode(final int index) {
        checkInterval(index, 0, size() - 1);
        return root.get(index);
    }

    private void setRoot(AVLNode node) {
        root = node;
        if (node != null) {
            node.parent = null;
        }
    }

    abstract protected boolean canAdd(E e);

    abstract protected void addNode(AVLNode node);

    abstract protected void removeNode(AVLNode node);

    private void checkInterval(final int index, final int startIndex, final int endIndex) {
        if (index < startIndex || index > endIndex) {
            throw new IndexOutOfBoundsException("Invalid index:" + index + ", size=" + size());
        }
    }

    void assertConsistent() {
        if (root == null) {
            assert(size() == 0);
        } else {
            assert(size() == root.countNodes());
        }
    }

    class AVLNode {
        private AVLNode parent;

        private AVLNode left;

        private boolean leftIsPrevious;

        private AVLNode right;

        private boolean rightIsNext;

        private int height;

        private int relativePosition;

        private E value;

        private AVLNode(final int relativePosition, final E obj, final AVLNode parent, final AVLNode rightFollower, final AVLNode leftFollower) {
            this.relativePosition = relativePosition;
            this.rightIsNext = true;
            this.leftIsPrevious = true;
            this.parent = parent;
            setRight(rightFollower);
            setLeft(leftFollower);
            setValue(obj);
        }

        E getValue() {
            return value;
        }

        void setValue(final E obj) {
            if (this.value != null) {
                removeNode(this);
            }
            this.value = obj;
            addNode(this);
        }

        AVLNode get(final int index) {
            final int indexRelativeToMe = index - relativePosition;

            if (indexRelativeToMe == 0) {
                return this;
            }

            final AVLNode nextNode = indexRelativeToMe < 0 ? getLeftSubTree() : getRightSubTree();
            if (nextNode == null) {
                return null;
            }

            return nextNode.get(indexRelativeToMe);
        }

        int getPosition() {
            int position = 0;
            AVLNode node = this;
            while (node != null) {
                position += node.relativePosition;
                node = node.parent;
            }
            return position;
        }

        void toArray(final Object[] array, final int index) {
            array[index] = value;
            if (getLeftSubTree() != null) {
                left.toArray(array, index + left.relativePosition);
            }
            if (getRightSubTree() != null) {
                right.toArray(array, index + right.relativePosition);
            }
        }

        AVLNode next() {
            if (rightIsNext || right == null) {
                return right;
            }

            return right.min();
        }

        AVLNode previous() {
            if (leftIsPrevious || left == null) {
                return left;
            }
            return left.max();
        }

        AVLNode insert(final int index, final E obj) {
            final int indexRelativeToMe = index - relativePosition;

            if (indexRelativeToMe <= 0) {
                return insertOnLeft(indexRelativeToMe, obj);
            }

            return insertOnRight(indexRelativeToMe, obj);
        }

        private AVLNode insertOnLeft(final int indexRelativeToMe, final E obj) {
            if (relativePosition >= 0) {
                relativePosition ++;
            }
            if (getLeftSubTree() == null) {
                setLeft(new AVLNode(-1,obj, this, this, left), null);
            } else {
                setLeft(left.insert(indexRelativeToMe, obj), null);
            }
            final AVLNode ret = balance();
            recalcHeight();
            return ret;
        }

        private AVLNode insertOnRight(final int indexRelativeToMe, final E obj) {
            if (relativePosition < 0) {
                relativePosition--;
            }
            if (getRightSubTree() == null) {
                setRight(new AVLNode(+1, obj, this, right,this), null);
            } else {
                setRight(right.insert(indexRelativeToMe, obj), null);
            }
            final AVLNode ret = balance();
            recalcHeight();
            return ret;
        }

        private AVLNode getLeftSubTree() {
            return leftIsPrevious ? null : left;
        }

        private AVLNode getRightSubTree() {
            return rightIsNext ? null : right;
        }

        private AVLNode max() {
            return getRightSubTree() == null ? this : right.max();
        }

        private AVLNode min() {
            return getLeftSubTree() == null ? this : left.min();
        }

        AVLNode remove(final int index) {
            final int indexRelativeToMe = index - relativePosition;

            if (indexRelativeToMe == 0) {
                return removeSelf(true);
            }
            if (indexRelativeToMe > 0) {
                setRight(right.remove(indexRelativeToMe), right.right);
                if (relativePosition < 0) {
                    relativePosition++;
                }
            } else {
                setLeft(left.remove(indexRelativeToMe), left.left);
                if (relativePosition > 0) {
                    relativePosition--;
                }
            }
            recalcHeight();
            return balance();
        }

        private AVLNode removeMax() {
            if (getRightSubTree() == null) {
                return removeSelf(false);
            }
            setRight(right.removeMax(), right.right);
            if (relativePosition < 0) {
                relativePosition++;
            }
            recalcHeight();
            return balance();
        }

        private AVLNode removeMin() {
            if (getLeftSubTree() == null) {
                return removeSelf(false);
            }
            setLeft(left.removeMin(), left.left);
            if (relativePosition > 0) {
                relativePosition--;
            }
            recalcHeight();
            return balance();
        }

        private AVLNode removeSelf(boolean removeValue) {
            removeNode(this);
            if (removeValue) {
                value = null;
            }
            if (getRightSubTree() == null && getLeftSubTree() == null) {
                return null;
            }
            if (getRightSubTree() == null) {
                if (relativePosition > 0) {
                    left.relativePosition += relativePosition + (relativePosition > 0 ? 0 : 1);
                }
                left.max().setRight(null, right);
                return left;
            }
            if (getLeftSubTree() == null) {
                right.relativePosition += relativePosition - (relativePosition < 0 ? 0 : 1);
                right.min().setLeft(null, left);
                return right;
            }

            if (heightRightMinusLeft() > 0) {
                final AVLNode rightMin = right.min();
                if (leftIsPrevious) {
                    setLeft(rightMin.left);
                }
                setRight(right.removeMin());
                if (relativePosition < 0) {
                    relativePosition++;
                }
                setValue(rightMin.value);
            } else {
                final AVLNode leftMax = left.max();
                if (rightIsNext) {
                    setRight(leftMax.right);
                }
                final AVLNode leftPrevious = left.left;
                setLeft(left.removeMax());
                if (left == null) {
                    leftIsPrevious = true;
                    setLeft(leftPrevious);
                }
                if (relativePosition > 0) {
                    relativePosition--;
                }
                setValue(leftMax.value);
            }
            recalcHeight();
            return this;
        }

        private AVLNode balance() {
            switch (heightRightMinusLeft()) {
                case 1:
                case 0:
                case -1:
                    return this;
                case -2:
                    if (left.heightRightMinusLeft() > 0) {
                        setLeft(left.rotateLeft(), null);
                    }
                    return rotateRight();
                case 2:
                    if (right.heightRightMinusLeft() <0 ) {
                        setRight(right.rotateRight(), null);
                    }
                    return rotateLeft();
                default:
                    throw new RuntimeException("Tree inconsistent");
            }
        }

        private int getOffset(final AVLNode node) {
            if (node == null) {
                return 0;
            }
            return node.relativePosition;
        }

        private int setOffset(final AVLNode node, final int newOffset) {
            if (node == null) {
                return 0;
            }
            final int oldOffset = getOffset(node);
            node.relativePosition = newOffset;
            return oldOffset;
        }

        private void recalcHeight() {
            height = Math.max(
                    getLeftSubTree() == null ? -1 : getLeftSubTree().height,
                    getRightSubTree() == null ? -1 : getRightSubTree().height) + 1;
        }

        private int getHeight(final AVLNode node) {
            return node == null ? -1 : node.height;
        }

        private int heightRightMinusLeft() {
            return getHeight(getRightSubTree()) - getHeight(getLeftSubTree());
        }

        private AVLNode rotateLeft() {
            final AVLNode newTop = right;
            final AVLNode movedNode = getRightSubTree().getLeftSubTree();

            final int newTopPosition = relativePosition + getOffset(newTop);
            final int myNewPostion = -newTop.relativePosition;
            final int movedPosition = getOffset(newTop) + getOffset(movedNode);

            setRight(movedNode, newTop);
            newTop.setLeft(this, null);

            setOffset(newTop, newTopPosition);
            setOffset(this, myNewPostion);
            setOffset(movedNode, movedPosition);
            return newTop;
        }

        private AVLNode rotateRight() {
            final AVLNode newTop = left;
            final AVLNode movedNode = getLeftSubTree().getRightSubTree();

            final int newTopPosition = relativePosition + getOffset(newTop);
            final int myNewPosition = -newTop.relativePosition;
            final int movedPosition = getOffset(newTop) + getOffset(movedNode);

            setLeft(movedNode, newTop);
            newTop.setRight(this, null);

            setOffset(newTop, newTopPosition);
            setOffset(this, myNewPosition);
            setOffset(movedNode, movedPosition);
            return newTop;
        }

        private void setLeft(final AVLNode node, final AVLNode previous) {
            leftIsPrevious = node == null;
            setLeft(leftIsPrevious ? previous : node);
            recalcHeight();
        }

        private void setLeft(final AVLNode node) {
            left = node;
            if (left != null && !leftIsPrevious) {
                left.parent = this;
            }
        }

        private void setRight(final AVLNode node, final AVLNode next) {
            rightIsNext = node == null;
            setRight(rightIsNext ? next : node);
            recalcHeight();
        }

        private void setRight(final AVLNode node) {
            right = node;
            if (right != null && !rightIsNext) {
                right.parent = this;
            }
        }

        private int countNodes() {
            int c = 1;
            if (!leftIsPrevious && left != null) {
                assert(left.parent == this);
                c += left.countNodes();
            }
            if (!rightIsNext && right != null) {
                assert(right.parent == this);
                c += right.countNodes();
            }

            return c;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                    .append("AVLNode(")
                    .append(relativePosition)
                    .append(',')
                    .append(left != null)
                    .append(',')
                    .append(value)
                    .append(',')
                    .append(getRightSubTree() != null)
                    .append(", faedelung ")
                    .append(rightIsNext)
                    .append(" )")
                    .toString();
        }
    }

    private class TreeListIterator implements ListIterator<E> {
        private final AbstractIndexedTreeList<E> parent;

        private AVLNode next;

        private int nextIndex;

        private AVLNode current;

        private int currentIndex;

        private int expectedModCount;

        protected TreeListIterator(final AbstractIndexedTreeList<E> parent, final int fromIndex) throws IndexOutOfBoundsException {
            super();
            this.parent = parent;
            this.expectedModCount = parent.modCount;
            this.next = parent.root == null ? null : parent.root.get(fromIndex);
            this.nextIndex = fromIndex;
            this.currentIndex = -1;
        }

        private void checkModCount() {
            if (parent.modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }

        public boolean hasNext() {
            return nextIndex < parent.size();
        }

        public E next() {
            checkModCount();
            if (!hasNext()) {
                throw new NoSuchElementException(" No element at index " + nextIndex + ".");
            }
            if (next == null) {
                next = parent.root.get(nextIndex);
            }
            final E value = next.getValue();
            current = next;
            currentIndex = nextIndex++;
            next = next.next();
            return value;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public E previous() {
            checkModCount();
            if (!hasPrevious()) {
                throw new NoSuchElementException("Already at start of the list");
            }
            if (next == null || next.previous() == null) {
                next = parent.root.get(nextIndex - 1);
            } else {
                next = next.previous();
            }
            final E value = next.getValue();
            current = next;
            currentIndex = --nextIndex;
            return value;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex() - 1;
        }

        public void remove() {
            checkModCount();
            if (currentIndex == -1) {
                throw new IllegalStateException();
            }
            parent.remove(currentIndex);
            if (nextIndex != currentIndex) {
                nextIndex--;
            }
            next = null;
            current = null;
            currentIndex = -1;
            expectedModCount++;
        }

        public void set(final E obj) {
            if (!supportSetInIterator) {
                throw new UnsupportedOperationException("Set operation is not supported");
            }
            if (canAdd(obj)) {
                checkModCount();
                if (current == null) {
                    throw new IllegalStateException();
                }
                current.setValue(obj);
            }
        }

        public void add(final E obj) {
            if (canAdd(obj)) {
                checkModCount();
                parent.add(nextIndex, obj);
                current = null;
                currentIndex = -1;
                nextIndex++;
                expectedModCount++;
            }
        }
    }
}
