package main.java.com.straven.datastructures.collections;

import java.util.*;
import java.util.function.Function;

public class IndexedTreeList<E> extends AbstractIndexedTreeList<E> {

    private final Comparator<AVLNode> NODE_COMPARATOR = Comparator.comparingInt(AVLNode::getPosition);
    private final Function<E, TreeSet<AVLNode>> NEW_NODE_TREE_SET = k -> new TreeSet(NODE_COMPARATOR);

    protected final Map<E, TreeSet<AVLNode>> nodeMap;

    public IndexedTreeList() {
        this(new HashMap<>());
    }

    public IndexedTreeList(final Map map) {
        this.nodeMap = map;
    }

    public IndexedTreeList(final Collection<? extends E> coll) {
        this(coll, new HashMap<>());
    }

    public IndexedTreeList(final Collection<? extends E> coll, final Map map) {
        this.nodeMap = map;
        for (E e: coll) {
            add(e);
        }
    }

    @Override
    public int indexOf(final Object object) {
        TreeSet<AVLNode> nodes = nodeMap.get(object);
        if (nodes == null || nodes.isEmpty()) {
            return -1;
        }
        return nodes.first().getPosition();
    }

    @Override
    public int lastIndexOf(final Object object) {
        TreeSet<AVLNode> nodes = nodeMap.get(object);
        if (nodes == null || nodes.isEmpty()) {
            return -1;
        }
        return nodes.last().getPosition();
    }

    public int[] indexes(final Object object) {
        TreeSet<AVLNode> nodes = nodeMap.get(object);
        if (nodes == null || nodes.isEmpty()) {
            return new int[0];
        }
        int[] indexes = new int[nodes.size()];
        int i = 0;
        for (AVLNode node : nodes) {
            indexes[i++] = node.getPosition();
        }
        return indexes;
    }

    public int count(final Object object) {
        TreeSet<AVLNode> nodes = nodeMap.get(object);
        if (nodes == null || nodes.isEmpty()) {
            return 0;
        }
        return nodes.size();
    }

    @Override
    public boolean contains(final Object object) {
        return nodeMap.containsKey(object);
    }

    @Override
    public void clear() {
        super.clear();
        nodeMap.clear();
    }

    public Set<E> uniqueValues() {
        return nodeMap.keySet();
    }

    @Override
    protected boolean canAdd(E e) {
        if (e == null) {
            throw new NullPointerException("Null elements are not allowed");
        }
        return true;
    }

    @Override
    protected void addNode(AVLNode node) {
        nodeMap.computeIfAbsent(node.getValue(), NEW_NODE_TREE_SET).add(node);
    }

    @Override
    protected void removeNode(AVLNode node) {
        TreeSet<AVLNode> nodes = nodeMap.remove(node.getValue());
        if (nodes == null) {
            return;
        }
        nodes.remove(node);
        if (!nodes.isEmpty()) {
            nodeMap.put(nodes.first().getValue(), nodes);
        }
    }
}
